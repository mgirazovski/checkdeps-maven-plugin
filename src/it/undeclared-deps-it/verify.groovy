import groovy.io.FileType;

final String buildLogFileName = "build.log";

File buildLogFile = new File( basedir, buildLogFileName);

assert buildLogFile.isFile(), "no build.log written";

String logContent =  buildLogFile.getText("UTF-8");

assert logContent.contains("Used but undeclared dependencies:"), "Used but undeclared dependencies should have been found";


