package com.ecircle.checkdeps.maven.plugin;

import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.dependency.analyzer.ProjectDependencyAnalysis;
import org.apache.maven.shared.dependency.analyzer.ProjectDependencyAnalyzer;
import org.apache.maven.shared.dependency.analyzer.ProjectDependencyAnalyzerException;

/**
 * 
 */
@Mojo( name = "checkdeps", requiresDependencyResolution = ResolutionScope.TEST, threadSafe = true, defaultPhase=LifecyclePhase.TEST_COMPILE  )
@Execute( phase = LifecyclePhase.TEST_COMPILE )
public class CheckDeps extends AbstractMojo {

    @Parameter(property = "project", required = true)
    private MavenProject project;

    @Component(role = ProjectDependencyAnalyzer.class)
    private ProjectDependencyAnalyzer analyzer;

    public void execute() throws MojoExecutionException {
        
        try {
            ProjectDependencyAnalysis analysis = analyzer.analyze(project);
            final Set<Artifact> usedUndeclaredArtifacts = analysis.getUsedUndeclaredArtifacts();

            if (!usedUndeclaredArtifacts.isEmpty()) {
                getLog().warn("Used but undeclared dependencies: (groupId:artifactId:type:version:classifier)");

                displayErrors(usedUndeclaredArtifacts);

                displayDependenciesExampleForUndeclaredDependencies(usedUndeclaredArtifacts);

                throw new MojoExecutionException("Used but undeclared dependencies found!");
            }

        } catch (ProjectDependencyAnalyzerException e) {
            getLog().error("Exception analyzing dependencies:", e);
            throw new MojoExecutionException("Exception analyzing dependencies:", e);
        }

    }

    private void displayErrors(final Set<Artifact> undeclaredUsedArtifacts) {
        for (Artifact artifact : undeclaredUsedArtifacts) {
            final String messageLine = "    " + artifact.getGroupId() + ":" + artifact.getArtifactId() + ":"
                    + artifact.getType() + ":" + artifact.getVersion() + ":" + artifact.getClassifier();
            getLog().warn(messageLine);
        }
    }

    private void displayDependenciesExampleForUndeclaredDependencies(final Set<Artifact> undeclaredUsedArtifacts) {
        getLog().warn("Example to include in your POM (please make sure to not overwrite <dependencyManagement> settings):");
        final StringBuilder xmlDependencyExample = new StringBuilder();
        xmlDependencyExample.append("\n");

        for (Artifact artifact : undeclaredUsedArtifacts) {
            xmlDependencyExample.append("    <dependency>\n" + "        <groupId>" + artifact.getGroupId()
                    + "</groupId>\n" + "        <artifactId>" + artifact.getArtifactId() + "</artifactId>\n"
                    + "        <version>" + artifact.getVersion() + "</version>\n");

            if (null != artifact.getType() && !"jar".equals(artifact.getType())) {
                xmlDependencyExample.append("        <type>" + artifact.getType() + "</type>\n");
            }
            if (null != artifact.getScope() && !Artifact.SCOPE_COMPILE.equals(artifact.getScope())) {
                xmlDependencyExample.append("        <scope>" + artifact.getScope() + "</scope>\n");
            }
            if (null != artifact.getClassifier()) {
                xmlDependencyExample.append("        <classifier>" + artifact.getClassifier() + "</classifier>\n");
            }

            xmlDependencyExample.append("    </dependency>\n");
        }

        getLog().warn(xmlDependencyExample.toString());
    }

}
